use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::fs::File;
// use std::io::LineWriter;
use std::io::Write;

pub struct Config {
    pub filename: String,
    pub key: usize,
    pub decrypt: u32,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 1 {
            return Err("not enough arguments");
        }
        let filename = String::from(&args[1]);
        let key: usize = match args[2].parse() {
            Ok(n) => n,
            Err(_) => {
                return Err("error: First argument is not a number");
            }
        };
        let decrypt: u32 = match args[3].parse() {
            Ok(n) => n,
            Err(_) => {
                return Err("error: Third argument is not a number");
            }
        };
        Ok(Config {
            key,
            filename,
            decrypt,
        })
    }
}

pub fn encrypt_decrypt(config: Config) -> Result<(), Box<dyn Error>> {
    let characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";
    let n = characters.chars().count();
    let key = if config.decrypt == 1 {
        n - config.key
    } else {
        config.key
    };
    // let mut file = File::create("encrypted.txt")?;
    // let mut file = File::create("decrypted.txt")?;
    let mut file = if config.decrypt == 1 {
        File::create("decrypted.txt")?
    } else {
        File::create("encrypted.txt")?
    };

    let shifted_characters = [&characters[key..], &characters[..key]].concat();
    let unicode_vec: Vec<_> = characters.chars().map(|c| c as u32).collect();
    let chars_vec: Vec<_> = shifted_characters.chars().map(|c| c as char).collect();
    let table = unicode_vec
        .into_iter()
        .zip(chars_vec)
        .collect::<HashMap<_, _>>();

    let f = fs::read_to_string(config.filename)?;
    let mut s = String::new();
    for line in f.lines() {
        let mut translated_vec: Vec<String> = Vec::new();
        let unicode_text_vec: Vec<_> = line.chars().map(|c| c as u32).collect();
        for i in unicode_text_vec {
            let translated_char = table.get(&i);
            translated_vec.push(translated_char.unwrap().to_string());
        }
        writeln!(file, "{}", translated_vec.join(""))?;
    }
    // println!("{:?}", translated_vec.join(""));
    // if config.decrypt == 0 {
    //     let mut file = File::create("encrypted.txt")?;
    //     // let text = String::from(translated_vec.join(""));
    //     write!(file, "{}", translated_vec.join(""));
    //     // file.write_all(translated_vec.join(""))?;
    // };
    // if config.decrypt == 1 {
    //     let mut file = File::create("decrypted.txt")?;
    //     // let text = String::from(translated_vec.join(""));
    //     write!(file, "{}", translated_vec.join(""));
    //     // file.write_all(translated_vec.join(""))?;
    // };
    // // let mut file2 = File::create("encrypted.txt")?;
    // // file2.write_all(translated_vec.join(""))?;

    Ok(())
}
