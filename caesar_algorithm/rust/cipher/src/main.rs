use cipher::Config;
use std::env;
use std::process;

fn cipher_text() {
    // def cipher_text(text,  key, characters=string.ascii_lowercase, decrypt=False):

}

fn main() {
    let args: Vec<String> = env::args().collect();
    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });
    let processed_text = cipher::encrypt_decrypt(config);

    // println!("{:?}", processed_text);
}
