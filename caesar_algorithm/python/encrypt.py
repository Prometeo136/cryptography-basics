# -*- coding: utf-8 -*-

import string
import argparse


def cipher_text(text,  key, characters=string.ascii_lowercase, decrypt=False):
    # string.ascii_lowercase returns a string of characters from 'a' to 'z'
    if key < 0:

        print("key cannot be negative")

        return None

    n = len(characters)
    if decrypt:
        key = n - key
    # str.maketrans method returns a mapping table that can be used with the translate() method to replace specified characters.
    # The slicing operation along with this new key ensures the character set has been left-shifted
    table = str.maketrans(characters, characters[key:]+characters[:key])
    translated_text = text.translate(table)
    return translated_text


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='encrypt and decript text')
    parser.add_argument('text',
                        metavar='text',
                        type=str,
                        help='Text to be encrypted')
    parser.add_argument('key',
                        metavar='key',
                        type=int,
                        help='Number of places a character will be shifted')
    parser.add_argument('mode',
                        metavar='mode',
                        type=int,
                        help='encrypt(1) or decrypt(0)')
    args = parser.parse_args()
    text = args.text
    key = args.key
    mode = True if args.mode == 1 else False

    character_set = string.ascii_lowercase + string.ascii_uppercase + \
        string.digits + " " + string.punctuation + 'ñ' + 'Ñ'
    output = cipher_text(text, key, character_set, decrypt=mode)
    if mode:
        message = 'Encrypted text:'
    else:
        message = 'Decrypted text:'
    print(message + '\n' + output)
