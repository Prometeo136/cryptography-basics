# Cipher algorithms in several programming languages

1. Caesar Cipher
---
The Caesar Cipher is one of the simplest and most widely known encryption techniques. It is a type of substitution cipher in which each letter in the plaintext is replaced by a letter some fixed number of positions down the alphabet.

Thus to cipher a given text we need an integer value, known as shift which indicates the number of position each letter of the text has been moved down.

The *Caesar Cipher* can be expressed mathematicaly as:

c = (x + n) % 27

Where c is the encoded character, x is the actual character, and n is the number of positions we want to shift the character x by. We’re taking mod with 27 because there are 27 letters in the Spanish alphabet.
